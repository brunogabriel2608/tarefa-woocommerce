
document.getElementById("billing_postcode").addEventListener("change", alteraCEP);
document.getElementById("billing_postcode").addEventListener("keyup", function(){
    var valor = document.getElementById("billing_postcode").value;
    var valorLimpo = valor.replace(/\D/g, '');
    if ((valorLimpo.toString().length) == 8) {
        alteraCEP();
    }
});

function alteraCEP() {
    // Pegando o valor do CEP
    var cep_bruto = document.getElementById("billing_postcode").value;
    //Nova variável "cep" somente com dígitos.
    var cep = cep_bruto.replace(/\D/g, '');
    //Verifica se campo cep possui valor informado.
    if (cep != "") {
        
        //Expressão regular para validar o CEP.
        var validacep = /^[0-9]{8}$/;

        //Valida o formato do CEP.
        if(validacep.test(cep)) {
            // Criando variável de requisição
            var request = new XMLHttpRequest()
            
            // Abrindo conexão
            request.open('GET', 'https://viacep.com.br/ws/'+ cep + '/json/', true)
            
            request.onload = function () {
                // Acessando os dados
                if (request.status >= 200 && request.status < 400) {
                    var data = JSON.parse(this.response);
                    document.getElementById("billing_address_1").value = data['logradouro'] + ', ' + data['bairro'];
                    document.getElementById("billing_city").value = data['localidade'];
                } else {
                    console.log('error');
                };
            }
            // Send request
            request.send()
        } //end if.
    } //end if.

}


document.getElementById('billing_first_name').placeholder = '   Digite seu nome';
document.getElementById('billing_last_name').placeholder = '   Digite seu sobrenome';
document.getElementById('billing_company').placeholder = '   Digite o nome da sua empresa';
document.getElementById('billing_phone').placeholder = '   (21) XXXXX-XXXX';
document.getElementById('billing_postcode').placeholder = '   XXXXX-XXX';
document.getElementById('billing_email').placeholder = '   Digite seu e-mail';

// Alterando nomes:
// Coluna 1, subtítulo
var lista = document.getElementsByClassName('woocommerce-billing-fields')
var lista2 = lista[0];
var lista3 = lista2.getElementsByTagName('h3')[0];
lista3.innerHTML = "Informações de entrega";

//=====================================================
let div = document.createElement("div");
let texto = `
    <div class='infoPagamento'>
    <h3>Informações de pagamento</h3>
        <div class='formasPagamento'>
            <h4>Forma de Pagamento</h4>
            <input type="checkbox" id="dinheiroRadio" name="pagamento" value="dinheiro" onclick="onlyOne(this)">
            <label for="dinheiroRadio">Dinheiro na Entrega</label><br>
            <input type="checkbox" id="cartaoRadio" name="pagamento" value="cartao" onclick="onlyOne(this)">
            <label for="cartaoRadio">Cartão</label>
        </div>
    </div>`;

div.innerHTML = texto;
let local = document.getElementsByClassName("woocommerce-billing-fields")[0];
local.appendChild(div);

let cartao = `
<div class='infoCartao'>
    <div class="input-text">
        <label>Número do cartão</label><br>
        <input type='text' placeholder="1111 1111 1111 1111" class = "pagamentoInput">
    </div>
    <div class='infoExtraCartao'>
        <div class='input-text'>
            <label>Validade do Cartão</label><br>
            <input type=text placeholder="12/12/2020" class = "pagamentoInput">
        </div>
        <div class='input-text'>
            <label>CVV</label><br>
            <input type=text placeholder="123" class = "pagamentoInput">
        </div>
    </div>
</div>`;


let local2 = document.getElementsByClassName("formasPagamento")[0];
console.log(local2);
let div2 = document.createElement("div");
div2.innerHTML = cartao;


var checkbox = document.querySelector("input[value=cartao]");
checkbox.addEventListener('change', function() {
    if (this.checked) {
        //console.log("Checkbox is checked..");
        local2.appendChild(div2);
    } else if(!(this.checked)) {
        //console.log("Checkbox is not checked..");
        div2.remove();
    }
});

var checkbox = document.querySelector("input[value=dinheiro]");
checkbox.addEventListener('change', function() {
    if (this.checked) {
        //console.log("Checkbox is not checked..");
        div2.remove();
    }
});


function onlyOne(checkbox) {
    var checkboxes = document.getElementsByName('pagamento');
    checkboxes.forEach((item) => {
        if (item !== checkbox) item.checked = false;
    })
}