carrinho = document.querySelector('.carrinho')
cart = document.querySelector('.cart')
closeCart = document.querySelector('.close-popup')
escurecer = document.querySelector('.escurecer-tela')
updateCart = document.querySelector('.button[name=update_cart]')
cartTotal = document.querySelector('.cart-total')
updateCart.setAttribute('id', 'update')
function cartClose() {
  cart.animate(
    {
      right: '-480px'
    },
    400
  )
  cart.style.right = '-480px'
  escurecer.style.visibility = 'hidden'
  return
}

carrinho.addEventListener('click', () => {
  cart.style.visibility = 'visible'
  cart.animate(
    {
      right: 0
    },
    400
  )

  cart.style.right = '0'
  escurecer.style.visibility = 'visible'
  escurecer.addEventListener('click', () => {
    cartClose()
    return
  })
  return
})

closeCart.addEventListener('click', () => {
  cartClose()
  cart.style.visibility = 'visible'
  escurecer.style.visibility = 'hidden'
  return
})

formQtdNode = document.querySelectorAll('.input-text.qty.text')
destinosNode = document.querySelectorAll(
  '.woocommerce-mini-cart-item.mini_cart_item'
)
updateButton = document.querySelector('.button[name=update_cart]')

formQtd = Array.from(formQtdNode)
destinos = Array.from(destinosNode)

for (let x = 0; x < formQtd.length; x++) {
  destinos[x].appendChild(formQtd[x])
}

orderTotal = document.querySelector('.cart-subtotal')
subTotal = teste1 = orderTotal.lastElementChild
cartTotal.appendChild(subTotal)
