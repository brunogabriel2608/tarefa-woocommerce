<?php
// Template Name: pagina inicial
?>

<?php get_header(); ?>
  <?php frontpage();?>
  <?php include 'carrinho.php';?>
   
  </section>
  <section class="introduction">
    <h1 class="title">Comes&Bebes</h1>
    <h4 class="subtitle">O restaurante para todas as fomes</h4>
  </section>

  <section class="productCategories">
    <h2>CONHEÇA NOSSA LOJA</h2>
    <div class="container">
      <p>Tipos de pratos principais</p>
      <?php echo do_shortcode('[product_categories]'); ?>
    </div>
  </section>

  <section class="dailyProduct">
    <p class="diadehoje">Pratos do dia de hoje</p>
    <p>
      <?php
        date_default_timezone_set('America/Sao_Paulo');
        $semana = date('l');
        
        switch($semana){
          case 'Monday':
            echo 'SEGUNDA';
            echo do_shortcode('[products limit="4" columns="4" tag="SEGUNDA"]');
            break;
        
          case 'Tuesday':
            echo 'TERÇA';
            echo do_shortcode('[products limit="4" columns="4" tag="TERCA"]');
            break;
          
          case 'Wednesday':
            echo 'QUARTA';
            echo do_shortcode('[products limit="4" columns="4" tag="QUARTA"]');
            break;
          
          case 'Thursday':
            echo 'QUINTA';
            echo do_shortcode('[products limit="4" columns="4" tag="QUINTA"]');
            break;
          case 'Friday':
            echo 'SEXTA';
            echo do_shortcode('[products limit="4" columns="4" tag="SEXTA"]');
            break;
          
          case 'Sunday':
            echo 'DOMINGO';
            echo do_shortcode('[products limit="4" columns="4" tag="DOMINGO"]');
            break;

          case 'Saturday':
            echo 'SÁBADO';
            echo do_shortcode('[products limit="4" columns="4" tag="SABADO"]');

          default:
            break;
          };
      ?>
    </p>
  

    <a href="http://comesbebes.local/shop/">
      <div class="divseeMore">
        <button class="seeMore">
          Veja outras opções
        </button>
      </div>
    </a>
  </section>

  <section class="prefooter">
    <h2>VISITE NOSSA LOJA FÍSICA</h2>
    <section class="images">
      <div class="local">
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14700.7537673028!2d-43.1332584!3d-22.9064193!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xfd5e35fb577af2f5!2sUFF%20-%20Instituto%20de%20Computa%C3%A7%C3%A3o!5e0!3m2!1spt-BR!2sbr!4v1660860266309!5m2!1spt-BR!2sbr" width="260" height="170" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade" class="map">
        </iframe>
      
        <p class="rua"> Rua lorem ipsum, 123, LI, Brasil</p>
        <p class="telefone">(XX) XXXX-XXXX</p>
      </div>
      <div class="carrossel">
        <!-- Slideshow container -->
        <div class="slideshow-container">
        <!-- Full-width images with number and caption text -->
        <div class="mySlides fade">
      
          <img src="http://s2.glbimg.com/2HAU3Q0GGs-VX14TcoBL8g7DBgo=/e.glbimg.com/og/ed/f/original/2014/10/10/466603763.jpg" width="350px" height="225px" >
          <!-- style="width:100%" -->
      
        </div>
        <div class="mySlides fade">
      
          <img src="https://scontent.fsdu9-1.fna.fbcdn.net/v/t1.6435-9/43669160_736279976712124_5337100802106327040_n.jpg?_nc_cat=101&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeHJqC8iW3UW6hjml5HU1Yti8ifesNxlppLyJ96w3GWmkvKJujCj-bQNGpH17iaQtRBsomh4-2PS64FvXUqhG0_c&_nc_ohc=Bw9xEjycu8UAX9pbbDd&_nc_ht=scontent.fsdu9-1.fna&oh=00_AT_JjWfRTKysbJDOKMKDFhLsDijoW58Sja1xg1sWDkW5Dw&oe=6321B5E3"  width="300px" height="200px" style="width:100%">
      
        </div>
        <div class="mySlides fade">
      
          <img src="https://www.emporiotambo.com.br/wp/wp-content/uploads/2019/09/padr%C3%A3o-grupo-de-pessoas-almo%C3%A7ando-em-restaurante.jpg" width="300px" height="200px" style="width:100%">
      
        </div>
        <!-- Next and previous buttons -->
        <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
        <a class="next" onclick="plusSlides(1)">&#10095;</a>
        </div>
        <br>
        <!-- The dots/circles -->
        <div style="text-align:center">
        <span class="dot" onclick="currentSlide(1)"></span>
        <span class="dot" onclick="currentSlide(2)"></span>
        <span class="dot" onclick="currentSlide(3)"></span>
        </div>
      </div>
    </section>
  </section>
  

  
<?php get_footer(); ?>