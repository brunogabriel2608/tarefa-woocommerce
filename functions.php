<?php


function my_pagination() {
global $wp_query;
echo paginate_links( array(
'base' => str_replace( 9999999999999, '%#%', esc_url( get_pagenum_link( 9999999999999 ) ) ),
'format' => '?paged=%#%',
'current' => max( 1, get_query_var( 'paged' ) ),
'total' => $wp_query->max_num_pages,
'type' => 'list',
'prev_next' => true,
'prev_text' => 'Anterior',
'next_text' => 'Próxima',
'before_page_number' => '',
'after_page_number' => '',
'show_all' => false,
'mid_size' => 1,
'end_size' => 1,
) );
}


function stylecss(){
wp_enqueue_style( 'style.css', get_template_directory_uri().'/style.css');
}





add_action( 'wp_enqueue_style', 'frontpage');
function frontpage(){
      wp_enqueue_style( 'front-page.css', get_template_directory_uri() . '/assets/css/front-page.css');
      wp_enqueue_script('front-page.js', get_template_directory_uri() . '/assets/js/front-page.js');
};

add_action('wp_enqueue_style', 'carrinho');
function carrinho(){
      wp_enqueue_style( 'carrinho.css', get_template_directory_uri() . '/carrinho.css');
      wp_enqueue_script('carrinho.js', get_template_directory_uri() . '/carrinho.js');
};
add_action( 'wp_enqueue_script', 'shoppage');
function shoppage(){
      wp_enqueue_script('shop.js', get_template_directory_uri() . '/assets/js/shop.js');
};

function checkoutpage(){
      wp_enqueue_style( 'checkout.css', get_template_directory_uri() . '/assets/css/checkout.css');
      wp_enqueue_script('checkout-page.js', get_template_directory_uri() . '/assets/js/checkout-page.js');
};

function productpage(){
      wp_enqueue_style( 'single-product.css', get_template_directory_uri() . '/assets/css/single-product.css');
};

function insercao_posts_index(){
      // Create post object
      $my_post1 = array();
      $my_post1['post_title']    = 'Categorias';
      $my_post1['post_content']  = '<h1 class="SelecioneCat"> SELECIONE UMA CATEGORIA </h1> <br> [product_categories]';
      $my_post1['post_status']   = 'publish';
      $my_post1['post_author']   = 1;
      // Insert the post into the database
      wp_insert_post( $my_post1 );

      // Create post object
      $my_post2 = array();
      $my_post2['post_title']    = 'Pratos';
      $my_post2['post_content']  = '<h1 class="pratos"> PRATOS </h1> <br> <input id="search" class="text" onkeyup="search()" placeholder="Buscar por nome"> <br> [products columns="4" limit="8" paginate="true" class="test"]';
      $my_post2['post_status']   = 'publish';
      $my_post2['post_author']   = 1;
      // Insert the post into the database
      wp_insert_post( $my_post2 );
}

function details(){
      ?>

<div class="editAcoount">
      <form class="woocommerce-EditAccountForm edit-account" action="" method="post" <?php do_action( 'woocommerce_edit_account_form_tag' ); ?> >
      
            <?php do_action( 'woocommerce_edit_account_form_start' ); ?>
      
            <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
                  <label for="account_first_name"><?php esc_html_e( 'First name', 'woocommerce' ); ?>&nbsp;</label>
                  <input placeholder=" Digite seu nome" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_first_name" id="account_first_name" autocomplete="given-name" value="<?php echo esc_attr( $user->first_name ); ?>" />
            </p>
            <p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
                  <label for="account_last_name"><?php esc_html_e( 'Last name', 'woocommerce' ); ?>&nbsp;</label>
                  <input placeholder=" Digite seu sobrenome" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_last_name" id="account_last_name" autocomplete="family-name" value="<?php echo esc_attr( $user->last_name ); ?>" />
            </p>
            <div class="clear"></div>
      
      
            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                  <label for="account_email"><?php esc_html_e( 'Email address', 'woocommerce' ); ?>&nbsp;</label>
                  <input placeholder="Digite seu email" type="email" class="woocommerce-Input woocommerce-Input--email input-text" name="account_email" id="account_email" autocomplete="email" value="<?php echo esc_attr( $user->user_email ); ?>" />
            </p>
      

      
                  <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <label for="password_current"><?php esc_html_e( 'Current password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>
                        <input placeholder="Digite sua senha atual" type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_current" id="password_current" autocomplete="on" />
                  </p>
                  <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <label for="password_1"><?php esc_html_e( 'New password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>
                        <input placeholder="Digite sua nova senha" type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_1" id="password_1" autocomplete="off" />
                  </p>
                  <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <label for="password_2"><?php esc_html_e( 'Confirm new password', 'woocommerce' ); ?></label>
                        <input placeholder="Confirme sua nova senha" type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_2" id="password_2" autocomplete="off" />
                  </p>

            <div class="clear"></div>
      
            <?php do_action( 'woocommerce_edit_account_form' ); ?>
      
            <div class="divbottom">
                  <?php wp_nonce_field( 'save_account_details', 'save-account-details-nonce' ); ?>
                  <button type="submit" class="woocommerce-Button button" name="save_account_details" value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>"><?php esc_html_e( 'Save changes', 'woocommerce' ); ?></button>
                  <input type="hidden" name="action" value="save_account_details" />
            </div>
      
            <?php do_action( 'woocommerce_edit_account_form_end' ); ?>
      </form>
</div>
<?php }
add_action( 'woocommerce_after_my_account', 'details' )


?>


