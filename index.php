<?php 
 // Template name: Loja
 ?>

<?php 
    get_header();
    stylecss();   
    shoppage();
?>

    <?php 
        if (!(have_posts())){
            insercao_posts_index();
        }
        ?>



        <?php if(have_posts()) {
                while(have_posts()) {
                    the_post();
                    //the_title();
                    the_content();
                }
        } 
        ?> 
        <div class="menu">
            <?php include 'carrinho.php'; ?>
        </div>
    <?php get_footer(); ?>